//
//  ViewController.h
//  Dragon Wars
//
//  Created by IMMANENT on 10/01/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
{
    
     IBOutlet UIWebView *webView;
     IBOutlet UIActivityIndicatorView *activityIndicator;
}
- (IBAction)leftBarButtonAction:(id)sender;

@end

