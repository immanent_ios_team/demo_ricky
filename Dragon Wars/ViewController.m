//
//  ViewController.m
//  Dragon Wars
//
//  Created by IMMANENT on 10/01/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()<UIWebViewDelegate>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     [[self navigationController] setNavigationBarHidden:YES animated:NO];
    [activityIndicator startAnimating];
    NSString *urlString = @"http://demodemo.cf:3000";
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [webView loadRequest:urlRequest];
}
-(void)webViewDidFinishLoad:(UIWebView *)webView {
    
   // [self removeLoadingView];
    NSLog(@"finish");
    [activityIndicator stopAnimating];

}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSString *currentURL = [[request URL] absoluteString];
    if ([currentURL isEqualToString:@"http://demodemo.cf:3000/"] || [currentURL isEqualToString:@"http://demodemo.cf:3000/login"]) {
        [self.navigationItem.leftBarButtonItem setTintColor:[UIColor clearColor]];
        [self.navigationItem.leftBarButtonItem setEnabled:NO];
        [[self navigationController] setNavigationBarHidden:YES animated:YES];

    }
    
    else {
        [[self navigationController] setNavigationBarHidden:NO animated:YES];

        [self.navigationItem.leftBarButtonItem setTintColor:[UIColor whiteColor]];
        [self.navigationItem.leftBarButtonItem setEnabled:YES];
    }
    return YES;
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
   // [self removeLoadingView];.
    [activityIndicator stopAnimating];

    NSLog(@"Error for WEBVIEW: %@", [error description]);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)leftBarButtonAction:(id)sender
{
    if ([webView canGoBack] ) {
        [webView goBack];
    }
}
@end
